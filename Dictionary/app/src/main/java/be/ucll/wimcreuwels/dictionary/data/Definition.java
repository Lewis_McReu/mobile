package be.ucll.wimcreuwels.dictionary.data;

import org.ksoap2.serialization.SoapObject;

/**
 * Created by Wim Creuwels on 6/11/2016.
 */

public class Definition {
    public final String word, definition;
    public final Dictionary dictionary;

    public Definition(String word, String definition, Dictionary dictionary) {
        this.word = word;
        this.definition = definition;
        this.dictionary = dictionary;
    }

    @Override
    public String toString() {
        return "From: " + dictionary.name + "/n" + definition;
    }

    public static Definition parse(SoapObject object) {
        String word = object.getPropertyAsString("Word");
        String definition = object.getPropertyAsString("WordDefinition");
        Dictionary dictionary = Dictionary.parse((SoapObject) object.getProperty("Dictionary"));
        return new Definition(word, definition, dictionary);
    }
}
