package be.ucll.wimcreuwels.dictionary;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import be.ucll.wimcreuwels.dictionary.data.Definition;
import be.ucll.wimcreuwels.dictionary.data.Dictionary;

/**
 * Created by Wim Creuwels on 28/10/2016.
 */

public class ServiceUtil {
    public static final String endpoint = "http://services.aonaware.com/DictService/DictService.asmx";
    public static final String namespace = "http://services.aonaware.com/webservices/";
    public static final String dictionaryListSoapAction = namespace + "DictionaryList";
    public static final String defineInDictSoapAction = namespace + "DefineInDict";
    public static final String defineSoapAction = namespace + "Define";

    public static List<Dictionary> getDictionaryList() throws IOException, XmlPullParserException {
        SoapObject object = new SoapObject(namespace, "DictionaryList");

        SoapSerializationEnvelope envelope = createEnvelope(object);
        HttpTransportSE ht = getHttpTransportSE();

        ht.call(dictionaryListSoapAction, envelope);
        SoapObject output = (SoapObject) envelope.getResponse();

        List<Dictionary> dicts = new ArrayList<>();

        for (int i = 0; i < output.getPropertyCount(); i++) {
            Dictionary dict = Dictionary.parse((SoapObject) output.getProperty(i));
            dicts.add(dict);
        }

        return dicts;
    }

    public static List<Definition> getDefinitions(String value) throws IOException, XmlPullParserException {
        if (value == null || value.isEmpty()) return Collections.emptyList();
        SoapObject object = new SoapObject(namespace, "Define");
        object.addProperty("word", value);

        SoapSerializationEnvelope envelope = createEnvelope(object);
        HttpTransportSE ht = getHttpTransportSE();
        ht.call(defineSoapAction, envelope);
        SoapObject output = (SoapObject) envelope.getResponse();
        List<Definition> defs = new ArrayList<>();
        SoapObject soapDefs = (SoapObject) output.getProperty("Definitions");
        for (int i = 0; i < soapDefs.getPropertyCount(); i++) {
            Definition def = Definition.parse((SoapObject) soapDefs.getProperty(i));
            defs.add(def);
        }

        return defs;
    }

    public static List<Definition> getDefinitionsInDict(String value, String dict) throws IOException, XmlPullParserException {
        if (value == null || value.isEmpty()) return Collections.emptyList();
        SoapObject object = new SoapObject(namespace, "DefineInDict");
        object.addProperty("word", value);
        object.addProperty("dictId", dict);

        SoapSerializationEnvelope envelope = createEnvelope(object);
        HttpTransportSE ht = getHttpTransportSE();
        ht.call(defineSoapAction, envelope);
        SoapObject output = (SoapObject) envelope.getResponse();
        List<Definition> defs = new ArrayList<>();
        SoapObject soapDefs = (SoapObject) output.getProperty("Definitions");
        for (int i = 0; i < soapDefs.getPropertyCount(); i++) {
            Definition def = Definition.parse((SoapObject) soapDefs.getProperty(i));
            defs.add(def);
        }

        return defs;
    }

    private static HttpTransportSE getHttpTransportSE() {
        HttpTransportSE ht = new HttpTransportSE(Proxy.NO_PROXY, endpoint, 60000);
        ht.setXmlVersionTag("<!--?xml version=\"1.0\" encoding= \"UTF-8\" ?-->");
        return ht;
    }

    private static SoapSerializationEnvelope createEnvelope(SoapObject request) {
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER12);
        envelope.dotNet = true;
        envelope.implicitTypes = true;
        envelope.setAddAdornments(false);
        envelope.setOutputSoapObject(request);
        return envelope;
    }
}
