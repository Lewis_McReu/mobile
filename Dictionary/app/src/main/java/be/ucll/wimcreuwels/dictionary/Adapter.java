package be.ucll.wimcreuwels.dictionary;

import android.widget.BaseAdapter;

import java.util.Collections;
import java.util.List;

/**
 * Created by Wim Creuwels on 6/11/2016.
 */

public abstract class Adapter<E> extends BaseAdapter {
    private List<E> list;

    public Adapter() {
        list = Collections.EMPTY_LIST;
    }

    public List<E> getList() {
        return list;
    }

    public void setList(List<E> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return position < getCount() ? list.get(position) : null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}
