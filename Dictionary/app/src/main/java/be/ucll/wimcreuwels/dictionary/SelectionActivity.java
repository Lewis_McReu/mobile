package be.ucll.wimcreuwels.dictionary;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import be.ucll.wimcreuwels.dictionary.data.Dictionary;

public class SelectionActivity extends AppCompatActivity {
    private DictionaryListTask activeTask;
    private ListView listView;
    private Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);

        adapter = new LocalAdapter();

        listView = (ListView) findViewById(R.id.dictionarylist);
        listView.setAdapter(adapter);

        activeTask = new DictionaryListTask();
        activeTask.execute();
    }

    private class DictionaryListTask extends AsyncTask<Void, Void, List<Dictionary>> {
        @Override
        protected List<Dictionary> doInBackground(Void... params) {
            try {
                return ServiceUtil.getDictionaryList();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }
            return Collections.EMPTY_LIST;
        }

        @Override
        protected void onPostExecute(List<Dictionary> dictionaries) {
            super.onPostExecute(dictionaries);
            activeTask = null;
            adapter.setList(dictionaries);
        }
    }

    private void selectDictionary(Dictionary dict) {
        setResult(0, new Intent().putExtra("id", dict.id).putExtra("name", dict.name));
        finish();
    }

    private class LocalAdapter extends Adapter<Dictionary> {
        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            Button b = (Button) convertView;
            if (convertView == null)
                b = createButton();
            b.setText(getList().get(position).name);
            b.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectDictionary(getList().get(position));
                }
            });
            return b;
        }
    }

    private Button createButton() {
        return new Button(this);
    }
}
