package be.ucll.wimcreuwels.dictionary;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import be.ucll.wimcreuwels.dictionary.data.Definition;
import be.ucll.wimcreuwels.dictionary.data.Dictionary;

public class LookupActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    private LocalAdapter adapter;
    private Dictionary selectedDict;
    private String currentWord;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lookup);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        entryView = (EditText) findViewById(R.id.lookup_entry);
        definitionList = (ListView) findViewById(R.id.definition_list);
        lookupButton = (Button) findViewById(R.id.lookup_button);
        adapter = new LocalAdapter();
        definitionList.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_send && currentWord != null) {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Definition of " + currentWord);
            emailIntent.putExtra(Intent.EXTRA_TEXT, adapter.getList().toString());
            startActivity(Intent.createChooser(emailIntent, "Send email..."));
        } else if (id == R.id.nav_select) {
            Intent i = new Intent(this, SelectionActivity.class);
            startActivityForResult(i, 0);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0 && resultCode == 0) {
            selectedDict = new Dictionary(data.getStringExtra("id"), data.getStringExtra("name"));
        } else
            super.onActivityResult(requestCode, resultCode, data);
    }


    private EditText entryView;
    private Button lookupButton;
    private ListView definitionList;

    public void onLookupClicked(View view) {
        String value = entryView.getText().toString();
        if (value == null || value.isEmpty())
            return;
        else {
            currentWord = value;
            if (selectedDict == null)
                activeTask = new DefineTask(value);
            else activeTask = new DefineInDictTask(value);
            activeTask.execute();
            InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE);

            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private DefineTask activeTask;

    private class LocalAdapter extends Adapter<Definition> {
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            LinearLayout lay = (LinearLayout) convertView;
            TextView def;
            TextView dic;
            if (lay == null) {
                lay = createLinearLayout();
                lay.setOrientation(OrientationHelper.VERTICAL);
                def = createTextView();
                dic = createTextView();
                lay.addView(dic);
                lay.addView(def);
            }
            def = (TextView) lay.getChildAt(1);
            dic = (TextView) lay.getChildAt(0);

            def.setText(getList().get(position).definition);
            dic.setText(getString(R.string.from_dict) + ": " + getList().get(position).dictionary.name);

            return lay;
        }
    }

    private class DefineTask extends AsyncTask<Void, Void, List<Definition>> {
        protected final String value;

        private DefineTask(String value) {
            this.value = value;
        }

        @Override
        protected List<Definition> doInBackground(Void... params) {
            try {
                return ServiceUtil.getDefinitions(value);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }
            return Collections.emptyList();
        }

        @Override
        protected void onPostExecute(List<Definition> definitions) {
            adapter.setList(definitions);
            activeTask = null;
        }
    }

    private class DefineInDictTask extends DefineTask {

        private DefineInDictTask(String value) {
            super(value);
        }

        @Override
        protected List<Definition> doInBackground(Void... params) {
            try {
                return ServiceUtil.getDefinitionsInDict(value, selectedDict.id);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }
            return Collections.emptyList();
        }
    }

    private TextView createTextView() {
        return new TextView(this);
    }

    private LinearLayout createLinearLayout() {
        return new LinearLayout(this);
    }
}
