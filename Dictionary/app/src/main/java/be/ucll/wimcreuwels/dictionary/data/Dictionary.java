package be.ucll.wimcreuwels.dictionary.data;

import org.ksoap2.serialization.SoapObject;

/**
 * Created by Wim Creuwels on 5/11/2016.
 */

public class Dictionary {
    public final String id, name;

    public Dictionary(String id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "id=" + id + ",name=" + name;
    }

    public static Dictionary parse(SoapObject object) {
        String id = object.getPropertyAsString("Id");
        String name = object.getPropertyAsString("Name");
        return new Dictionary(id, name);
    }
}
