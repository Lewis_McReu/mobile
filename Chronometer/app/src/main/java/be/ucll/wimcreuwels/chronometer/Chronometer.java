package be.ucll.wimcreuwels.chronometer;

import android.content.SharedPreferences;

/**
 * Created by Wim Creuwels on 21/10/2016.
 */

public class Chronometer {
    private long start;
    private long end;
    private boolean paused;

    public Chronometer() {
        start = -1;
        end = start;
        paused = true;
    }

    public void toggle() {
        paused = !paused;
        if (!paused) {
            start = now();
            if (end > -1) {
                start -= end;
            }
        } else {
            end = now() - start;
        }
    }

    public void reset() {
        start = -1;
        end = start;
        if (!paused) start = now();
    }

    public void load(SharedPreferences prefs) {

    }

    public void save(SharedPreferences.Editor editor) {
        editor.putLong("start", start);
        editor.putLong("end", end);
        editor.putBoolean("paused", paused);
        paused = true;

        editor.commit();
    }

    private long now() {
        return System.currentTimeMillis();
    }

    public boolean isPaused() {
        return paused;
    }

    @Override
    public String toString() {
        long value = 0;
        if (start > 0) {
            value = now() - start;
            if (paused) {
                value = end;
            }
        }
        String format = "%02d:%02d:%02d:%03d";

        long millis = value % 1000;
        long secs = (value / 1000) % 60;
        long mins = ((value / 1000) / 60) % 60;
        long hrs = ((value / 1000) / 60) / 60;

        return String.format(format, hrs, mins, secs, millis);
    }
}
