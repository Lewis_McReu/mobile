package be.ucll.wimcreuwels.chronometer;

import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    private TextView timeView;
    private Button startStopButton;

    private Chronometer chrono;
    private Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        timeView = (TextView) findViewById(R.id.timeView);
        startStopButton = (Button) findViewById(R.id.startStopButton);
        chrono = new Chronometer();
        handler = new Handler();
        loadState();
    }

    private void updateTimeView() {
        timeView.setText(chrono.toString());
    }

    public void onStartStopClicked(View view) {
        chrono.toggle();
        if (!chrono.isPaused()) onTick();
        updateTextView();
    }

    public void onResetClicked(View view) {
        chrono.reset();
        updateTimeView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        loadState();
        onTick();
    }

    private void onTick() {
        if (!chrono.isPaused()) {
            updateTimeView();
            handler.postDelayed(new Runnable() {
                public void run() {
                    onTick();
                }
            }, 80);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        saveState();
    }

    private void saveState() {
        chrono.save(PreferenceManager.getDefaultSharedPreferences(this).edit());
    }

    private void loadState() {
        chrono.load(PreferenceManager.getDefaultSharedPreferences(this));

        updateTimeView();
        updateTextView();
    }

    private void updateTextView() {
        startStopButton.setText(chrono.isPaused() ? R.string.start : R.string.stop);
    }
}
